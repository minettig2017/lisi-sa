#ifndef OPERADORES_H
#define OPERADORES_H
#include <vector>

using namespace std;

class Operadores
{
    public:
        Operadores(){};
        vector<int> intercambio (vector<int> vec, int cant);
        vector<int> inversion (vector<int> vec);
        vector<int> insercion (vector<int> vec);
        vector<int> tresSwap (vector<int> vec);

        vector<int> intercambioILS (vector<int> vec, int pos);
        vector<int> giantLeap (vector<int> vec,int  nu_move);
       /* Solucion intercambio (Solucion s);
        Solucion inversion (Solucion s);
        Solucion insercion (Solucion s);
        Solucion tresSwap (Solucion s);
        */
        vector<int> newOperator (vector<int> sol,vector<int> desp, int Med_placas);

    private:
};

#endif // OPERADORES_H
