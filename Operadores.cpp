#include "../include/Operadores.h"
#include <time.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

//realiza "cant" intercambios en el vector
vector<int> Operadores::intercambio(vector<int> vec, int cant){
//Solucion Operadores::intercambio(Solucion s){
   while(cant>0){
    int i = (int) rand()%vec.size();
    int f = (int) rand()%vec.size();
    if (i!=f){
        int aux = vec[i];
        vec[i] = vec[f];
        vec[f] =aux;

        cant--;
        }
    }

    return vec;
}

vector<int> Operadores::inversion (vector<int> vec){
//Solucion Operadores::inversion(Solucion s){
    int ini = (int) rand()%(vec.size());
    int fin = (int) ( ini + (rand()%(vec.size()-ini)) );

    vector<int> aux;
    for (int i = ini; fin>i; i++){
        aux.insert(aux.end(),vec[i]);
    }

    for(ini; ini!=fin; ini++){
        vec[ini] = aux.back();
        aux.pop_back();
    }
    return vec;
}

vector<int> Operadores::insercion (vector<int> vec){
//Solucion Operadores::insercion(Solucion s){
    int ini = (int) rand()%(vec.size());
    int fin = (int) ( ini + (rand()%(vec.size()-ini)) );

    int aux = vec[fin];
    fin--;
    for (fin ; fin!= (ini-1) ; fin --){
        vec[fin+1] = vec[fin];
    }
    vec[ini] = aux;

    return vec;
}

vector<int> Operadores::tresSwap (vector<int> vec){
//Solucion Operadores::tresSwap(Solucion s){
    int posUno = (int) rand()%vec.size();
    int posDos=0;
    int posTres=0;
    do{
        posDos = (int) rand()%vec.size();
    }while (posUno==posDos);
    do{
         posTres = (int) rand()%vec.size();
    }while(posUno==posTres || posDos==posTres);

    int uno = vec[posUno];
    int dos = vec[posDos];
    vec[posUno] = vec[posTres];
    vec[posDos] = uno;
    vec[posTres] = dos;

    return vec;
}

vector<int> Operadores::intercambioILS (vector<int> vec, int pos){
    int i = (int) rand()%vec.size();
    if (pos!=i){
        int aux = vec[i];
        vec[i] = vec[pos];
        vec[pos] =aux;
        }else{
           vec = intercambioILS(vec,pos);
        }

    return vec;
}

vector<int> Operadores::giantLeap (vector<int> vec, int nu_move){
    for (int i=0; i < nu_move; i++){
            vec = inversion(vec);
    }
    return vec;
}

vector<int> Operadores::newOperator (vector<int> sol,vector<int> desp, int Med_placas){
	int bad=0;
    int bad_pos=0;
    //obtengo la pos donde mas desperdicio tengo
    for (int i=0;i<desp.size();i++){
        if(bad<desp[i]){
            bad=desp[i];
            bad_pos=i;
        }
    }
    //==========================================================
    //copiado de como calculo desperdicio
    int sum=0;
    int aux=0;
    bool bandera =true;
    for (unsigned int i=0; i<sol.size();i++){
            //init = i;
            aux= sum + sol[i];
            if (aux<=Med_placas){
                sum=aux;
            }else{
                if((Med_placas-sum) == bad){
                /*
                    encontrar el primer elemento que sea grande como bad o menor igual */
					                   /* while (bad!=0){
					                        for (int j=(i+1);j<sol.size();j++){
					                            if(sol[j]==bad){
					                                sol[i] = sol [j];
					                                bad=0;
					                                j=sol.size();
					                                i=j;
					                            }
					                        }
					                        bad--;
					                    }*/
                		while(bandera){
	                		int pos = (int) ( i + (rand()%(sol.size()-i)) );
	                		if (sol[pos]<=bad){
	                			sol[i] = sol[pos];
	                			bandera=false;
	                		}
						}
                    }else{
                    sum=0;
                    i--;
                }
            }
    }
    //===========================================================
    return sol;
}
