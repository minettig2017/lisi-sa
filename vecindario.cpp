#include "Vecindario.hpp"

list <Solucion>::iterator Vecindario::generar_vecindario(Solucion *s_seed, int tam_v){ //tam_v => tamaño del vecindario generado
  while (tam_v > 0){
    Operador op;
    Solucion s_nueva;
    s_nueva = *s_seed;
    int i, *indice;
    indice = s_nueva.get_vector();
    int p1 = op.generar_punto(0, s_nueva.get_tam()-2);
    int p2 = op.generar_punto(p1+1, s_nueva.get_tam()-p1-1);
    op.intercambiar(&indice[p1], &indice[p2]);
    l_soluciones.push_back(s_nueva);
    tam_v--;
  }
  return l_soluciones.begin();
}

list <Solucion> Vecindario::get_list() { return l_soluciones;}
