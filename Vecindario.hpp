#ifndef VECINDARIO_H
#define VECINDARIO_H

#include "operador.cpp"
#include "Solucion.hpp"

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <list>

using namespace std;

class Vecindario{
  private:
    list <Solucion> l_soluciones;
  public:
    list <Solucion>::iterator generar_vecindario(Solucion *, int tam_v);
    list <Solucion> get_list();
};

#endif
