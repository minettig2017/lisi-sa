#include "Generador.hpp"

Generador::Generador(int tam){ _tam = tam;}

Generador::Generador(int tam_i, int tam_j){ //_tam_i => cantidad de clientes //_tam_j => cantidad de camiones
  _tam_i = tam_i;
  _tam_j = tam_j;
}

int * Generador::generar_vector(){
  Operador op;
  int *vector, i, p1, p2;
  vector = new int[_tam];
  for(i=0; i<_tam; i++)
    vector[i] = i+1;
  for(i=0; i<_tam; i++){
    p1 = op.generar_punto(0, _tam-2);
    p2 = op.generar_punto(p1+1, (_tam-1)-p1);
    op.intercambiar(&vector[p1], &vector[p2]);
  }
  return vector; //devuelve un puntero al primer elemento del vector
}

int * Generador::generar_vector(int inicio, int fin){
  Operador op;
  int *vector, i, p1, p2, tam = fin-inicio;
  vector = new int[tam];
  for(i=0; i<tam; i++){
    vector[i] = inicio+1;
    inicio++;
  }
  for(i=0; i<tam; i++){
    p1 = op.generar_punto(0, tam-2);
    p2 = op.generar_punto(p1+1, (tam-1)-p1);
    op.intercambiar(&vector[p1], &vector[p2]);
  }
  return vector; //devuelve un puntero al primer elemento del vector
}
