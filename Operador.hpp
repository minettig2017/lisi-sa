#ifndef OPERADOR_H
#define OPERADOR_H

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "Solucion.hpp"
class Solucion;


using namespace std;

class Operador {
  public:
    void (Operador::*pBuscar[3])(int *, int *);
  public:
    Operador();
    int generar_punto (int, int);
    void insertar (int *, int *);
    void invertir (int *, int *);
    void intercambiar (int *, int *);
    void intercambiar (int *, int *, int *);
    void buscar(int, int *, int *);//el primer parámetro es para seleccionar el tipo de búsqueda
    void leap(Solucion &);
    void busqueda_local(Solucion &);
};

#endif
