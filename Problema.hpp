#ifndef PROBLEMA_H
#define PROBLEMA_H

#include "Solucion.hpp"
#include "Estadistica.hpp"
#include "Generador.hpp"
class Solucion;

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <list>
#include <vector>

using namespace std;

struct Camion{
  int *puntero;
  int id;
  float carga;
  double odometro;
};

class Problema{
  private:
    static int *_demanda; //vector con la demanda de cada cliente
    static double **_distancia; //matriz con las distancias entre puntos
    static int _rutas; //cantidad de camiones o rutas
    static int _clientes; //cantidad de clientes
    static float _capacidad; //capacidad de cada camión
    static int _sobrecarga; //cantidad de camiones sobrecargados
  public:
    static void inicializar(int, char*);
    static int rutas();
    static int clientes();
    static int capacidad();
    static double evaluar(int*);
    // static void evaluar(Solucion*);
    static int * explorar(int*);
    static int comprobar(int*);
    static void invertir(int*);
    static void invertirPuro(int*);
    static void intercambiar(int*);
    static void insertar(int*);
    static void nOpt(int*, int);
    static void giantLeap(int*, int);
  private:
    static struct Camion * seleccionar_camiones(int*);
    static void calcular_distancia(int*, int*);
    static double penalizar(double);
    // static void penalizar(Solucion*);
    static void sumar_carga(int, float&);
    static void restar_carga(int, float&);
    static double sumar_distancia(int, int, double);
};
#endif
