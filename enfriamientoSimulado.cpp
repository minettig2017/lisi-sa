#include "../include/EnfriamientoSimulado.h"
#include "math.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

EnfriamientoSimulado::EnfriamientoSimulado(int op,int pro){
    opera = op;
    probNro = pro;

    //cargo el problema
    prob.problemaNRO(probNro);
    solucActual.cargarProblema(prob);
    SolucAlternativa.cargarProblema(prob);
    /*
    prob (probNro);
    solucActual (prob);
    SolucAlternativa (prob);
    */
        //cargo la primera solucion
    solucActual.solucionInicialGreedy();

}

void EnfriamientoSimulado::calcular(clock_t start){
    //declaraciones
    int cantItera=0, bestItera=0;
    double coc, bestTime;
    long double s = 0;
    clock_t MSfin;
    //seteo la temp inical
    temp = set_temp_inicial();

    do{
      do{
            cantItera ++;
            switch(opera){
            case 1:
            SolucAlternativa.nuevaSolucion(oper.intercambio(solucActual.getSolucionActual(),2));
            break;
            case 2:
            SolucAlternativa.nuevaSolucion(oper.inversion(solucActual.getSolucionActual()));
            break;
            case 3:
            SolucAlternativa.nuevaSolucion(oper.insercion(solucActual.getSolucionActual()));
            break;
            case 4:
            SolucAlternativa.nuevaSolucion(oper.tresSwap(solucActual.getSolucionActual()));
            break;
            default:
            SolucAlternativa.nuevaSolucion(oper.intercambio(solucActual.getSolucionActual(),1));
            break;
            }

            s = SolucAlternativa.getCosto() - solucActual.getCosto();
            if(s < 0 || ( (exp((double) s/temp)) < (rand()/RAND_MAX) ) ){
                solucActual = SolucAlternativa;
                bestItera=cantItera;
                MSfin = clock();
            }

        } while ((cantItera%10) == 0);
        coc = (double)cantItera / (cantItera+1);
        //coc = (double) log (k+1) / log (k+2) ;
        //coc =(double) ( exp (k) / exp (k+1) );

        temp = coc * temp;
        }while (temp > 0.0001 && cantItera < 50000);
    costoActual = solucActual.getCosto();
    bestTime= ((double) (MSfin - start)) / CLOCKS_PER_SEC;
    cout<<costoActual<<", "<<temp<<", "<<cantItera<<", "<<bestItera<<", "<<bestTime;
    //delete solucActual;
    //delete SolucAlternativa;
    //delete prob;
}

double EnfriamientoSimulado::set_temp_inicial(){
    const double BETA  = 1.05;
    const double TEST  = 10;
    const double ACRAT = .8;
    double ac;
    double temperature = 1.0;
    do{
            temperature *=BETA;
            ac = 0;

            for (int i=0; i < TEST; i++){
                switch(opera){
                case 1:
                SolucAlternativa.nuevaSolucion(oper.intercambio(solucActual.getSolucionActual(),2));
                break;
                case 2:
                SolucAlternativa.nuevaSolucion(oper.inversion(solucActual.getSolucionActual()));
                break;
                case 3:
                SolucAlternativa.nuevaSolucion(oper.insercion(solucActual.getSolucionActual()));
                break;
                case 4:
                SolucAlternativa.nuevaSolucion(oper.tresSwap(solucActual.getSolucionActual()));
                break;
                default:
                SolucAlternativa.nuevaSolucion(oper.intercambio(solucActual.getSolucionActual(),1));
                break;
            }
                long double s = solucActual.getCosto() - SolucAlternativa.getCosto();
                if (s > 0 || ( (exp((double) s/temperature)) > (rand()/RAND_MAX) )){
                    solucActual = SolucAlternativa;
                    ac += 1.0/test;
                }
            }

        } while (ac < acrat);

    return  temperature;
}

EnfriamientoSimulado::~EnfriamientoSimulado(){
    temp = 0;
    costoActual = 0;
    probNro = 0;
    opera = 0;
//    delete solucActual;
//    delete SolucAlternativa;
//    delete oper;
//   delete prob;

}
