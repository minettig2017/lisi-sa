#include "Problema.hpp"

int Problema::_rutas = 0;
int Problema::_clientes = 0;
int *Problema::_demanda = NULL;
double **Problema::_distancia = NULL;
float Problema::_capacidad = 0;
int Problema::_sobrecarga = 0;

void Problema::inicializar(int c_rutas, char *archivo){
  // ofstream coord ("coord.m", ios::app);
  int i=0, j=0;
  int *x, *y;
  double foptimo = 0;
  _rutas = c_rutas;
  ifstream instancia(archivo);
  if(instancia.is_open()){
    instancia >> _clientes;
    instancia >> foptimo;
    instancia >> _capacidad;
    cout << "CLIENTES-----: " << _clientes << endl;
    cout << "CAPACIDAD-----: " << _capacidad << endl;
    cout << "FOPTIMO-----: " << foptimo << endl;
    Estadistica::inicializar(foptimo);
    x = new int[_clientes+1];
    y = new int[_clientes+1];
    _distancia = new double*[_clientes+1];
    for(i=0; i < _clientes + 1; i++)
      _distancia[i] = new double[_clientes+1];
    _demanda = new int[_clientes+1];
    while(!instancia.eof()){
      instancia >> x[j];
      instancia >> y[j];
      instancia >> _demanda[j];
      j++;
    }
    instancia.close();
  }
  // for(int i=0; i<_clientes+1; i++){
  //   coord << x[i] << " ";
  // }
  // coord << "\n";
  //
  // for(int i=0; i<_clientes+1; i++){
  //   coord << y[i] << " ";
  // }
  // coord << "\n";
  // coord.close();
  calcular_distancia(x, y);
  delete[] x;
  delete[] y;
}

int * Problema::demanda(){ return _demanda;}

double ** Problema::distancia(){ return _distancia;}

int Problema::rutas(){ return _rutas;}

int Problema::clientes(){ return _clientes;}

int Problema::capacidad(){ return _capacidad;}

void Problema::calcular_distancia(int *x, int *y){
  // ofstream file ("distancia.csv");
  int i, j;
  for(i = 0; i < (_clientes + 1); i++){
    for(j = 0; j < (_clientes + 1) ; j++){
  	  _distancia[i][j] = sqrt(pow((double) (x[j] - x[i]),2) + pow((double) (y[j] - y[i]),2));
      // file << _distancia[i][j] << ";";
    }
    // file << "\n";
  }
}

struct Camion * Problema::seleccionar_camiones(Solucion *s){
  struct Camion *camion = new Camion[_rutas];
  int *solucion = s->vector();
  int j=0;
  for(int i=0; i < s->size(); i++){
    if(solucion[i] > _clientes){
      camion[j].puntero = &solucion[i];
      camion[j].id = solucion[i];
      camion[j].carga = 0;
      camion[j].odometro = 0;
      j++;
    }
  }
  return camion;
}

void Problema::evaluar(Solucion *s){
 struct Camion *camion = seleccionar_camiones(s);

 int cont = 0;
 double distancia = 0;

 int *inicio = s->elemento(0);
 int *fin = s->elemento(_clientes+_rutas-1);
 int *actual = camion[cont].puntero;
 int *siguiente = actual+1;

  while(cont < _rutas){
    sumar_carga(*siguiente, camion[cont].carga);
    if(camion[cont].carga > _capacidad) _sobrecarga++;

    camion[cont].odometro = sumar_distancia(*actual, *siguiente, camion[cont].odometro);

    if(*siguiente > _clientes){
      distancia += camion[cont].odometro;
      cont++;
    }

    if(siguiente == fin){
      actual = siguiente;
      siguiente = inicio;
    }
    else{
     actual = siguiente;
     siguiente++;
    }
  }
  s->fitness(distancia);
  if(_sobrecarga > 0) penalizar(s);
  Estadistica::calcular_fa(s->fitness());
  Estadistica::incrementar_cev();
}

void Problema::penalizar(Solucion *s){
  Estadistica::incrementar_cpn();
  s->fitness(s->fitness() + (_sobrecarga*500));
  _sobrecarga = 0;
}

int * Problema::explorar(int *vector, int size){
  Operador op;
  int id = rand() % 3;
  // int chance = rand() % 100;
  int p1,p2;
  p1 = op.generar_punto(0,size-2);
  p2 = op.generar_punto(p1+1, (size-1)-p1);

  op.buscar(id, &vector[p1], &vector[p2]);
  return vector;
}

int Problema::comprobar(Solucion *s){
  cout << "---------------------------" << endl;

  struct Camion *camion = seleccionar_camiones(s);
  int penalizado = 0;
  int cont = 0;

  int *inicio = s->elemento(0);
  int *fin = s->elemento(_clientes+_rutas-1);
  int *actual = camion[cont].puntero;
  int *siguiente = actual+1;

  // ofstream file ("recorrido.csv");
  while(cont < _rutas){
    sumar_carga(*siguiente, camion[cont].carga);
    // file << *actual << " ; " << *siguiente ;

    if(*siguiente <= _clientes){
      // file << " ; DEMANDA ; " << _demanda[*siguiente] ;
    }
    else{
      // file << " ; DEMANDA ; 0 " ;
    }

    if(*actual > _clientes){
      if(*siguiente <= _clientes){
        // file << " ; DISTANCIA ; " <<  _distancia[0][*siguiente];
      }
      else if(*siguiente > _clientes){
        // file << " ; DISTANCIA ; 0 ";
      }
    }
    else if(*actual <= _clientes){
      if(*siguiente <= _clientes){
        // file << " ; DISTANCIA ; " <<  _distancia[*actual][*siguiente];
      }
      else if(*siguiente > _clientes){
        // file << " ; DISTANCIA ; " <<  _distancia[*actual][0];
      }
    }

    // file << endl;
    if(camion[cont].carga > _capacidad) penalizado++;

    camion[cont].odometro = sumar_distancia(*actual, *siguiente, camion[cont].odometro);
    //cout << *actual << " --> " << *siguiente << " --> CARGA --> " << camion[cont].carga << " --> ODOMETRO --> " << camion[cont].odometro << endl;

    if(*siguiente > _clientes){
      cout << "RUTA--: " << cont << endl;
      cout << "CAMION--: " << cont << " DISTANCIA--> " << camion[cont].odometro << endl;
      cont++;
    }

    if(siguiente == fin){
      actual = siguiente;
      siguiente = inicio;
    }
    else{
     actual = siguiente;
     siguiente++;
    }
  }
  // file.close();
  cout << "PENALIZACIONES--> " << penalizado << endl;
  if(penalizado > 0){
     cout << "SOLUCION NO VALIDA" << endl;
     return 1;
  }
  else{
     cout << "SOLUCION VALIDA" << endl;
     return 0;
  }
}

void Problema::sumar_carga(int i, float &carga){
  if(i <= _clientes) carga = carga + _demanda[i];
}

void Problema::restar_carga(int i, float &carga){
  if(i <= _clientes) carga = carga - _demanda[i];
}

double Problema::sumar_distancia(int i, int j, double d_acum){
  if(i > _clientes){
    if(j <= _clientes){
      d_acum = d_acum + _distancia[0][j];
    }
    else if(j > _clientes);
  }
  else if(i <= _clientes){
    if(j <= _clientes){
      d_acum = d_acum + _distancia[i][j];
    }
    else if(j > _clientes){
      d_acum = d_acum + _distancia[i][0];
    }
  }
  return d_acum;
 }
