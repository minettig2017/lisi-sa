#include "../include/Solucion.h"
#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

void Solucion::cargarProblema(Problema p){

    prob = p;
    Med_placas = prob.getMed_placas();
    //calc_cost();
}

/*
void Solucion::solucionInicial(vector<int> m, vector<int> c){
    //cargo en un vector auxiliar
    vector<int> aux;
    for(unsigned int i=0; i<m.size(); i++){
        for(int j=0;j<c[i];j++){
            aux.push_back(m[i]);
        }
    }
    while(0<aux.size()){
         int h = rand() % (aux.size());
         soluc.push_back(aux[h]);
         aux.erase(aux.begin()+h);
    }
    calc_cost();
}
*/
 //void Solucion::solucionInicialGreedy(vector<int> m, vector<int> c){
 void Solucion::solucionInicialGreedy(){
    vector<int> aux;
    int kaux=0;
    long double cost_aux=0;
    int cant_art = 0;
    vector<int> c = prob.getCant_cortes();
    vector<int> m = prob.getMed_cortes();
    //obtengo la cantidad de cortes a realizar
    for (int j=0; j<c.size(); j++ ){
        cant_art+=c[j];
    }

    //cargo el primero
    int i = (int) rand()%m.size();
 //ver esta resta porque ahora no manejo el vectos sino la clase que tiene el vector
    c[i]--;

    soluc.push_back(m[i]);
    if (c[i]==0){
       c.erase (c.begin()+(i));
       m.erase (m.begin()+(i));
    }
    //elijo los otros
    for (int h=1; h<cant_art; h++){
        soluc.push_back(m[0]);
        calc_cost();
        aux = soluc;
        cost_aux = costo;
        for (int k=1; k<m.size();k++){
            soluc[h] = m[k];
            calc_cost();
            if (costo < cost_aux){
                aux = soluc;
                cost_aux = costo;
                kaux = k;
            }
        }
            soluc =aux;
            calc_cost();
            c[kaux]--;
            if (c[kaux]==0){
                c.erase (c.begin()+(kaux));
                m.erase (m.begin()+(kaux));
            }
            kaux=0;
    }
 }

void Solucion::desperdicio(){
    int sum=0;
    int aux=0;
    desper.clear();
    for (unsigned int i=0; i<soluc.size();i++){
            aux= sum + soluc[i];
            if (aux<=Med_placas){
                sum=aux;
            }else{
                desper.push_back(Med_placas-sum);
                sum=0;
                i--;
            }
    }
    desper.push_back(Med_placas-sum);
}

void  Solucion::nuevaSolucion(vector<int> vec){
    soluc.clear();
    desper.clear();
    int i=0;
    for (i=0;i<vec.size();i++){
        soluc.push_back(vec[i]);
    }
    calc_cost();
}


void Solucion::mostrar(){
    cout<<"El orden de los cortes es: "<<endl;
    int aux = 0;
    for (unsigned int i=0; i<soluc.size();i++){
        aux = aux + soluc[i];
        if (aux > Med_placas){
            i--;
            aux = 0;
            cout<<"- ";
        }else{
            cout<< soluc[i]<<" ";
        }
    }
    cout<<endl;
/*
    cout<<"El desperdicio que se produce por perfil: "<<endl;
    for (unsigned int i=0; i<desper.size();i++){
        cout<< desper[i]<<" ";
    }
    */
    cout<<endl;
}

void Solucion::calc_cost(){
    costo=0;
    desperdicio();
    long double aux =0;
    long double b = (long double) Med_placas;
    long double cTres=0;
    long double cDos=0;
    long double cUno=1./(desper.size()+1);
    for(unsigned int i=0;i<desper.size();i++){
        if (desper[i]!=0){
            cTres+=1./desper.size();
            //aux++;
        }
    }
    //cTres = aux /desper.size();
    for(unsigned int i=0;i<desper.size();i++){
        long double a=desper[i];
        aux=sqrt(a/b);
        cDos+=aux;
    }
    costo=cUno*(cDos+cTres);
}

//sobrecargar operador =
Solucion& Solucion::operator=(const Solucion& s){
    this->costo = s.getCosto();

    this->soluc.clear();
    for (int i=0; i < s.getSolucionActual().size(); i++){

        this->soluc.push_back(s.getSolucionActual()[i]);
    }
    this->desper.clear();
    for (int i=0; i < s.getDesperdicio().size(); i++){
        this->desper.push_back(s.getDesperdicio()[i]);
    }

    return *this;
}

Solucion::~Solucion() {
   soluc.empty();
   desper.empty();
   costo = 0;
}
