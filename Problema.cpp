#include "../include/Problema.h"
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <sstream>
#include <fstream>

using namespace std;
/*
Problema::Problema(int prob){

}*/

void Problema::problemaNRO(int prob){
 ifstream fichin;
    switch(prob){
            case 1:
             fichin.open("./cortes/cortes1a.txt");
            break;
            case 2:
             fichin.open("./cortes/cortes2a.txt");
            break;
            case 3:
             fichin.open("./cortes/cortes3a.txt");
            break;
            case 4:
             fichin.open("./cortes/cortes4a.txt");
            break;
            case 5:
             fichin.open("./cortes/cortes5a.txt");
            break;
            }
        string linea;
        if (!fichin){
            cout << endl << "IMPOSIBLE LEER EL ARCHIVO DE LOS CORTES A REALIZAR";
            exit(0);
        }
        else
        {
            getline(fichin,linea); //LEE LAS MEDIDAS DE LOS CORTES
            stringstream ss(linea);
            string token;
            while (getline(ss,token, ' '))
            {
                int c=atoi(token.c_str());
                med_cortes.push_back(c);
            }
            getline(fichin,linea); //LEE LA CANTIDAD DE CORTES DE CADA MEDIDA
            stringstream dd(linea);
            while (getline(dd,token, ' '))
            {
                int c=atoi(token.c_str());
                cant_cortes.push_back(c);
            }
            getline(fichin,linea); //LEE LA MEDIDAD DE LAS PLACAS
            stringstream ff (linea);
            while (getline(ff,token, ' '))
            {
                int c=atoi(token.c_str());
                med_placas=c;
            }

            fichin.close();
        } // Fin del
}

void Problema::imprimirCortes(){
    cout<<"CORTES"<<endl;
    for(unsigned int i=0;i<med_cortes.size();i++){
        cout<<med_cortes[i]<<" ";
    }
    cout<<" "<<endl;
    for(unsigned int i=0;i<cant_cortes.size();i++){
        cout<<cant_cortes[i]<<" ";
    }
    cout<<" "<<endl;
}


void Problema::imprimirPlacas(){
    /*cout<<"PLACAS"<<endl;
    for(unsigned int i=0;i<med_placas.size();i++){
        cout<<med_placas[i]<<" ";
    }
    cout<<" "<<endl;*/
    cout<<"EL TAMAÑO DE LAS PLACAS ES DE: "<<med_placas<<endl;
}

Problema& Problema::operator=(const Problema& p){
    med_placas = p.getMed_placas();

    for (int i=0; i< p.getCant_cortes().size();i++){
        cant_cortes.push_back(p.getCant_cortes()[i]);
    }

    for (int i=0; i< p.getMed_cortes().size();i++){
        med_cortes.push_back(p.getMed_cortes()[i]);
    }

    return *this;
}



Problema::~Problema(){
    med_cortes.empty();
    cant_cortes.empty();
    med_placas=0;
}
