#ifndef SOLUCION_H
#define SOLUCION_H
#include <vector>
#include "Problema.h"

using namespace std;

class Solucion
{
    public:
        Solucion (){};
//        Solucion(Problema p);
        void cargarProblema(Problema p);
        //void solucionInicial(vector<int> med, vector<int> cant);
        void solucionInicialGreedy();
        //get
        vector<int> getSolucionActual() const{return soluc;}
        vector<int> getDesperdicio() const{return desper;}
        long double getCosto() const{return costo;}
        int getMed_placas() const {return Med_placas;}
        Problema getProblema() const{return prob;}

        void nuevaSolucion(vector<int> vec);
        void mostrar();

        Solucion& operator= (const Solucion& s);
        ~Solucion();

    private:
        void desperdicio();
        void calc_cost();
        vector<int> soluc;
        //vector<int> placas;
        vector<int> desper;
        int Med_placas;
        long double costo;
        Problema prob;
};

#endif // SOLUCION_H
