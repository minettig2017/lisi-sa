#ifndef BANDADA_H
#define BANDADA_H

#include "Solucion.hpp"
#include "Generador.hpp"
#include "Estadistica.hpp"

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <list>

using namespace std;

class Bandada{
  private:
    Solucion *_lider;
    list <Solucion> _r_izq;
    list <Solucion> _r_der;
    int count;
  public:
    Bandada();
    Solucion * lider();
    list <Solucion> * rder();
    list <Solucion> * rizq();
    void generar(int, int);
    void mover();
    void saltar();
};

#endif
