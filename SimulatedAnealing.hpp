#ifndef SIMULEATEDANEALING_H
#define SIMULEATEDANEALING_H

#include "Solucion.hpp"

using namespace std;

class SimulatedAnealing
{
    public:
        SimulatedAnealing(char **);

        Solucion explorar();

        ~SimulatedAnealing();

    private:
        double set_temp_inicial(Solucion);

    private:
        double coc;
        double e;
        Solucion s_inicial;
        Solucion s_nueva;
};

#endif
