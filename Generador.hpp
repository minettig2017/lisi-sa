#ifndef GENERADOR_H
#define GENERADOR_H

#include "Operador.hpp"

#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

class Generador {
  private:
    int _tam;
    int _tam_i;
    int _tam_j;
  public:
    Generador(int);
    Generador(int, int);
    int * generar_vector();
    int * generar_vector(int, int);
};

#endif
