#ifndef ESTADISTICA_H
#define ESTADISTICA_H

#include <iostream>
#include <stdlib.h>

#include "Solucion.hpp"
class Solucion;

using namespace std;

class Estadistica{
  private:
    static double _foptimo; //fitness optimo
    static Solucion _ms; //mejor solución hallada
    static double _tms; //tiempo en el que se halló mejor_s
    static int _ims; //iteracion en la que se halló mejor_s
    static double _tmr; //timer
    static clock_t _stmr; //tiempo inicio
    static clock_t _ftmr; //tiempo fin
    static double _fp; //fitness promedio
    static double _fa; //fitness acumulado
    static float _gap; //gup de la mejor solucion
    static float _agap; //gup promedio
    static int _cs; //cantidad de soluciones generadas;
    static int _ci; //cantidad de iteraciones
    static int _cev; //cantidad de evaluaciones realizadas
    static int _crp; //cantidad de soluciones reparadas
    static int _cpn; //cantidad de penalizaciones;
    static float _ef; //cantidad de soluciones optimas sobre el total (optimas + reparadas)
    static int _valida;
    static double fitness_ant;
    static ofstream _fs;
    static ofstream _fo;
  public:
    static void inicializar(double);
    static void guardar_tms(float); //guarda el tiempo en el cual se hallo ms
    static void guardar_ims(int); //guarda la iteracion en la cual se hallo ms
    static void guardar_ms(Solucion); //guarda la mejor solucion encontrada
    static int gitr(); //devuelve la cantidad de iteraciones hasta el momento
    static int gims(); //devuelve la iteracion en que se halló la mejor solución
    static void stimer(); //guarda el tiempo de inicio
    static void ftimer(); //guarda el tiempo de fin
    static float gtimer(); //devuelve el tiempo transcurrido hasta la llamada a la funcion
    static void timer(); //setea el tiempo transcurrido entre _stmr y _ftmr
    static void mostrar_ms(); //muestra el vector solucion hallado
    static void escribir_ms(); //escribe en el archivo "recorrido" la mejor solución
    static Solucion * ms(); //devuelve la mejor solución
    static void incrementar_ci(); //incrementa la cantidad de iteraciones
    static void incrementar_cs(); //incrementa la cantidad de soluciones generadas
    static void incrementar_cev(); //incrementa la cantidad de soluciones evaluadas
    static void incrementar_crp(); //incrementa la cantidad de soluciones reparadas
    static void incrementar_cpn(); //incrementa la cantidad de soluciones penalizadas
    static void calcular_fp(); //calcula el fitness promedio
    static void calcular_fa(double); //calcula el fitness acumulado
    static void calcular_ef(); //calcula la efectividad
    static void gap(); //calcula el gup de la ms
    static float gap_value(); //calcula el gup de la ms
    static void agap(); //calcula el gup promedio
    static float umbral();
    static void comparar(Solucion *);//compara la mejor solucion existente con la hallada hasta el momento
    static void header();
    static void psumarize(); //almacenaniemto parcial de los datos
    static void sumarize(); //almacenamiento datos finales
    static void guardar_fs(double);
    static void guardar_fo(double);
};

#endif
