#include "SimulatedAnealing.hpp"

SimulatedAnealing::SimulatedAnealing(char *argv []){
    double coc = 0;
    double e = 0;
    srand(time(NULL));
    Problema::inicializar(atoi(argv[0]), argv[1]);
    Estadistica::header();
}

Solucion SimulatedAnealing::explorar(){
    //Inicializo el cronometro
    Estadistica::stimer();
    //Genero y evaluo, dentro del constructor, la solucion inicial
    Solucion s_inicial(Problema::clientes() + Problema::rutas());
    Solucion s_nueva = s_inicial;
    int temp = set_temp_inicial(s_nueva);
    int k = 0;
    do{
      do{
            k++;
            //genero la nueva solucion a partir de los operadores de intercambio, inserción e inversión
            s_nueva.vector(Problema::explorar(s_nueva.vector()));
            //evalúo la nueva solución
            s_nueva.fitness(Problema::evaluar(s_nueva.vector()));

            e = s_nueva.fitness() - s_inicial.fitness();
            if(e <= 0 || ( (exp((double) e / temp)) < (rand() / RAND_MAX))){
                s_inicial = s_nueva;
                Estadistica::guardar_ims(k);
                //guardar tiempo de mejor solucion
                Estadistica::guardar_tms(Estadistica::gtimer());
                Estadistica::guardar_ms(s_inicial);
                Estadistica::psumarize();
            }

        } while ((k % 10) == 0);
        coc = (double)k / (k + 1);
        temp = temp - coc;
        //temp = coc * temp; //actualizar temp = temp - coc
    }while (temp > 0.0001 && k < 50000);
    return s_inicial;
}

double SimulatedAnealing::set_temp_inicial(Solucion s_nueva){
    //inicializar la temperatura con un solo operador;
    const double BETA  = 1.05;
    const double TEST  = 10;
    const double ACRAT = .8;
    double ac;
    double temperature = 1.0;
    do{
        temperature *= BETA;
        ac = 0;

        for (int i=0; i < TEST; i++){
            s_nueva.vector(Problema::explorar(s_nueva.vector()));

            s_nueva.fitness(Problema::evaluar(s_nueva.vector()));
            double s = s_inicial.fitness() - s_nueva.fitness();
            if (s > 0 || ( (exp((double) s/temperature)) > (rand()/RAND_MAX) )){
                s_inicial = s_nueva;
                ac += 1.0/TEST;
            }
        }
    } while (ac < ACRAT);

    return  temperature;
}

SimulatedAnealing::~SimulatedAnealing(){
}
