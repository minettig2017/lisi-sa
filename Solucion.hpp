#ifndef SOLUCION_H
#define SOLUCION_H

#include "Operador.hpp"
#include "Problema.hpp"
#include "Generador.hpp"
#include "Estadistica.hpp"
class Problema;
class Operador;

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <list>

using namespace std;

class Solucion{
  private:
    int *_vector; //puntero al primer elemento del vector solucion
    list <Solucion> _vecindario;
    double _fitness; //fitness de la solucion
    int _tam; //tamaño del vector asociado a la solucion
  public:
    Solucion();
    Solucion(int);
    void inicializar(int *);
    void vector(int *); //setea el vector solucion
    int * vector() const; //devuelve el vector solucion
    int * elemento(int);
    void fitness(double); //setea el fitness de la solucion
    double fitness() const; //devuelve el fitness de la solucion
    void size(int); //setea el tamaño de la solucion
    int size() const; // devuelve el tamaño de la solucion
    list <Solucion> * vecindario();
    Solucion & operator=(const Solucion &);
    void generar_vecindario(int, int);
    void transferir_xms(list<Solucion>::iterator);
    void transferir_xms(list<Solucion>::iterator, list<Solucion>::iterator);
    static void comparar(Solucion &, list<Solucion> *);
    void vaciar_vecindario();
    void mostrar_vecindario();
  private:
    void insertar_s(Solucion *s, int x);//inserta una nueva solucion en el vecindario de forma ordenada
    void guardar_info();

};

#endif
