#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <list>

#include "Problema.hpp"
#include "Solucion.hpp"
#include "Operador.hpp"
#include "Generador.hpp"
#include "Estadistica.hpp"
#include "SimulatedAnealing.hpp"

using namespace std;

int main(int argc, char *argv[]){
	SimulatedAnealing sa(argv);
	Solucion s;
	s = sa.explorar();

    Estadistica::guardar_ms(s);
	Estadistica::sumarize();
	Estadistica::mostrar_ms();
	cout << "SOLUCION FINAL: " << s.fitness() << endl;
}
