#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>

#include "Problema.hpp"
#include "Solucion.hpp"
#include "Operador.hpp"
#include "Bandada.hpp"
#include "Generador.hpp"

#include <list>
using namespace std;

int main (){
  srand(time(NULL));
  list<Solucion>::iterator indice_d;
  list<Solucion>::iterator indice_i;
  Problema::inicializar(10, "Taillard/tai75b.dat");
  Bandada bandada;
  bandada.generar(11, 85);

  Problema::evaluar(bandada.lider());
  cout << "LIDER--:" << bandada.lider()->fitness() << endl;
  cout << "RAMA IZQUIERDA" << endl;
  indice_i = bandada.rizq()->begin();
  while(indice_i != bandada.rizq()->end()){
    cout << indice_i->fitness() << endl;
    indice_i++;
  }

  cout << "RAMA DERECHA" << endl;
  indice_d = bandada.rder()->begin();
  while(indice_d != bandada.rder()->end()){
    cout << indice_d->fitness() << endl;
    indice_d++;
  }
}
