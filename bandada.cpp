#include "Bandada.hpp"

Bandada::Bandada(){
    count = 1;
}

Solucion * Bandada::lider(){ return _lider;}

list <Solucion> * Bandada::rder(){ return &_r_der;}

list <Solucion> * Bandada::rizq(){ return &_r_izq;}

void Bandada::generar(int tam_b, int tam_i){ //tam_b => tamaño de la bandada // tam_i => tamaño de cada vector asociado una solucion
  Generador g(tam_i);
  _lider = new Solucion(tam_i);
  _lider->vector(g.generar_vector());
  Problema::evaluar(_lider);

  Estadistica::incrementar_cs();

  while(tam_b-1 > 0){
    _r_izq.push_back(Solucion(tam_i));
    _r_izq.back().vector(g.generar_vector());
    Problema::evaluar(&_r_izq.back());

    Estadistica::incrementar_cs();

    _r_der.push_back(Solucion(tam_i));
    _r_der.back().vector(g.generar_vector());
    Problema::evaluar(&_r_der.back());

    Estadistica::incrementar_cs();
    tam_b = tam_b-2;
  }
}

void Bandada::mover(){
  if(count == 1){
    _r_izq.push_back(*_lider);
    *_lider = _r_izq.front();
    _r_izq.pop_front();
    count = 0;
  }
  else if(count == 0){
    _r_der.push_back(*_lider);
    *_lider = _r_der.front();
    _r_der.pop_front();
    count = 1;
  }
}
