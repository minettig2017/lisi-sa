#include "Solucion.hpp"

Solucion::Solucion(){
    _vector = NULL;
    _fitness = 0;
    _tam = 0;
}

Solucion::Solucion(int tam){
    Generador g(tam);
    _vector = g.generar_vector();
    // _vector = new int[tam];
    // _fitness = 0;
    _fitness = Problema::evaluar(_vector);
    _tam = tam;
}

void Solucion::vector(int *p){ _vector = p;}

int * Solucion::vector() const {  return _vector;}

int * Solucion::elemento(int i){ return &_vector[i];}

void Solucion::fitness(double f){  _fitness = f;}

double Solucion::fitness() const {  return _fitness;}

void Solucion::size(int tam){  _tam = tam;}

int Solucion::size() const {  return _tam;}

list <Solucion> * Solucion::vecindario(){ return &_vecindario;}

Solucion & Solucion::operator=(const Solucion &s){
  if(this != &s){
    //int *vector_s = s.vector();
    _tam = s.size();
    _vector = new int [_tam];
    memcpy(_vector, s.vector(), _tam*sizeof(int));
    //memcpy(_vector, vector_s, _tam*sizeof(int));
    _fitness = s.fitness();
  }
  return *this;
}

void Solucion::generar_vecindario(int tam_v, int x){ //x representa al vector de soluciones compartidas entre las aves
  Operador op;
  x = x + 1;
  insertar_s(this, x);
  while (tam_v > 0){
    Solucion *s_nueva = new Solucion();
    *s_nueva = *this;
    s_nueva->vector(Problema::explorar(s_nueva->vector()));
    s_nueva->fitness(Problema::evaluar(s_nueva->vector()));

    // Problema::evaluar(s_nueva);

    Estadistica::incrementar_cs();

    insertar_s(s_nueva, x);
    tam_v--;
    delete s_nueva;
  }
}

void Solucion::insertar_s(Solucion *s, int x){
  list <Solucion>::iterator it = _vecindario.begin();
  if(_vecindario.empty()){
    _vecindario.push_back(*s);
  }
  else{
    while(it != _vecindario.end()){
      if(s->fitness() <= it->fitness()){
        _vecindario.insert(it,*s);
        if(_vecindario.size() > x)
          _vecindario.pop_back();
        break;
      }
      else
        it++;
    }
    if(it == _vecindario.end() && _vecindario.size() < x){
      _vecindario.push_back(*s);
    }
  }
}

void Solucion::transferir_xms(list<Solucion>::iterator s){
  Operador op;
  while(this->_vecindario.size() > 0){
    s->_vecindario.push_back(this->_vecindario.front());
    this->_vecindario.pop_front();
  }
}

void Solucion::transferir_xms(list<Solucion>::iterator s_d, list<Solucion>::iterator s_i){
  Operador op;
  while(this->_vecindario.size() > 0){
    s_d->_vecindario.push_back(this->_vecindario.front());
    this->_vecindario.pop_front();

    s_i->_vecindario.push_back(this->_vecindario.front());
    this->_vecindario.pop_front();
  }
}

void Solucion::comparar(Solucion &s, list<Solucion> *vecindario){
  Estadistica::comparar(&vecindario->front());
  if(s.fitness() == vecindario->front().fitness()){
    vecindario->pop_front();
  }
  else if(vecindario->front().fitness() < s.fitness()){
    s = vecindario->front();
    vecindario->pop_front();
  }
}

void Solucion::vaciar_vecindario(){
  if(_vecindario.size() > 0)
    _vecindario.erase(_vecindario.begin(), _vecindario.end());
}

void Solucion::mostrar_vecindario(){
  list<Solucion>::iterator it = _vecindario.begin();
  int i = 1;
  cout << "SOLUCIONES ENCONTRADAS EN EL VECINDARIO" << endl;
  while(it != _vecindario.end()){
    cout << "fitness solucion "<< i << ": " << it->fitness() << endl;
    it++; i++;
  }
}

void Solucion::inicializar(int* vector){
  _vector = vector;
  // _fitness = Problema::evaluar(_vector);
}
