#ifndef ENFRIAMIENTOSIMULADO_H
#define ENFRIAMIENTOSIMULADO_H
#include <time.h>
#include "Problema.h"
#include "Solucion.h"
#include "Operadores.h"


class EnfriamientoSimulado
{
    public:
        EnfriamientoSimulado(){};
        EnfriamientoSimulado(int op, int problema);
        //void cargarParametros(int op, int problema);

        void calcular(clock_t start);
        virtual ~EnfriamientoSimulado();
        long double getCostoActual(){return costoActual;}
        long double getTempActual(){return temp;}

    private:
        double temp;
        long double costoActual;
        double set_temp_inicial();
        Problema prob;
        Solucion solucActual;
        Solucion SolucAlternativa;
        Operadores oper;
        int probNro;
        int opera;

};

#endif // ENFRIAMIENTOSIMULADO_H
