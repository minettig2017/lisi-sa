#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <list>

#include "Problema.hpp"
#include "Solucion.hpp"
#include "Operador.hpp"
#include "Bandada.hpp"
#include "Generador.hpp"
#include "Estadistica.hpp"

using namespace std;

class MBO{
  private:
    Bandada bandada;
    int _K;
    int _k;
    int _x;
    int _n;
    int _tam_i;
    int _m;
  public:
    void inicializar(char *argv[]);
    void ejecutar();
    void mostrar(Bandada &);
};

void MBO::inicializar(char *argv[]){
  _K = atoi(argv[1]);
  _k = atoi(argv[2]);
  _x = atoi(argv[3]);
  _n = atoi(argv[4]);
  _m = atoi(argv[5]);
  srand(time(NULL));
  Problema::inicializar(atoi(argv[6]), argv[7]);
  _tam_i = Problema::clientes() + Problema::rutas();//quitar de aca
  //Estadistica::header();
}

void MBO::ejecutar(){
  Estadistica::stimer();
  int j, valor = _K/4;
  int i=0;
  bandada.generar(_n, _tam_i); //n => tamaño de la bandada y tam_i => tamaño del vector asociado a las soluciones
  Solucion *lider = bandada.lider();
  list<Solucion> *s_d = bandada.rder();
  list<Solucion> *s_i = bandada.rizq();
  list <Solucion>::iterator indice_d; //indice_d => iterador rama derecha
  list <Solucion>::iterator indice_i; //indice_i => iterador rama izqueirda
  list<Solucion>::iterator d_siguiente; //indice que apunta al siguiente elemento de la rama derecha
  list<Solucion>::iterator i_siguiente; //indice que apunta al siguiente elemento de la rama izquierda
  while(i<_K){
    for(j=0; j<_m; j++){
      indice_d = s_d->begin();
      indice_i = s_i->begin();
      d_siguiente = indice_d;
      i_siguiente = indice_i;
      d_siguiente++; i_siguiente++;
      lider->generar_vecindario(_k, _x*2);
      Solucion::comparar(*lider, lider->vecindario());
      lider->transferir_xms(indice_d, indice_i);
      i = i + _k;
      while(1){
        if(indice_d != s_d->end()){
          indice_d->generar_vecindario(_k-_x, _x);
          Solucion::comparar(*indice_d, indice_d->vecindario());
          if(d_siguiente != s_d->end())
            indice_d->transferir_xms(d_siguiente);
          else if(d_siguiente == s_d->end())
            indice_d->vaciar_vecindario();
          indice_d++;
          d_siguiente++;
        }
        else if(indice_i != s_i->end()){
          indice_i->generar_vecindario(_k-_x, _x);
          Solucion::comparar(*indice_i, indice_i->vecindario());
          if(i_siguiente != s_i->end())
            indice_i->transferir_xms(i_siguiente);
          else  if(i_siguiente == s_i->end())
            indice_i->vaciar_vecindario();
          indice_i++;
          i_siguiente++;
        }
        else if(indice_d == s_d->end() && indice_i == s_i->end()){
          break;
        }
      }
      i = i + (_k-_x) * (_n-1);
    }
    Estadistica::escribir_ms();
    bandada.mover();
    Estadistica::psumarize();
    Estadistica::incrementar_ci();
  }
  Estadistica::ftimer();
  Estadistica::sumarize();
  Estadistica::mostrar_ms();
}

void MBO::mostrar(Bandada &bandada){
  cout << "-----------------------------------" << endl;
  int i, cont = 0;
  list<Solucion> *s_d = bandada.rder();
  list<Solucion> *s_i = bandada.rizq();
  list <Solucion>::iterator indice_d = s_d->begin(); //indice_d => iterador rama derecha
  list <Solucion>::iterator indice_i = s_i->begin(); //indice_i => iterador rama izqueirda
  cout << "LIDER" << endl;

  cout << bandada.lider()->fitness() << endl;

  cout << "ELEMENTOS DE LA DERECHA" << endl;
  while(indice_d != s_d->end()){
    cout << indice_d->fitness() << endl;
    indice_d++;
  }

  cout << "ELEMENTOS DE LA IZQUIERDA " << endl;
  while(indice_i != s_i->end()){
    cout << indice_i->fitness() << endl;
    indice_i++;
  }
}

int main(int argc, char *argv[]){
  MBO mbo;
  mbo.inicializar(argv);
  mbo.ejecutar();
}
