#include "Operador.hpp"

#define BETA 10

Operador::Operador(){
  pBuscar[0] = &Operador::insertar;
  pBuscar[1] = &Operador::invertir;
  pBuscar[2] = &Operador::intercambiar;
}

void Operador::buscar(int i, int *pa, int *pb){
  if(i<0 || i > 3) return;
  (this->*pBuscar[i])(pa, pb);
}

int Operador::generar_punto(int inicio, int fin){
  int x = inicio + rand() % fin;
  return x;
}

void Operador::insertar(int *pa, int *pb){
  int *indice;
  if(pa < pb){
    indice = pb - 1;
    while (pa != indice){
      intercambiar(indice, pb);
      pb = indice;
      indice--;
    }
  }
  else{
    indice = pb + 1;
    while (pa != pb){
      intercambiar(indice, pb);
      pb = indice;
      indice++;
    }
  }
}

void Operador::invertir(int *pa, int *pb){
  while(pa < pb){
    intercambiar(pa, pb);
    pa++;
    pb--;
  }
}

void Operador::intercambiar (int *pa, int *pb){
  int aux = 0;
  aux = *pa;
  *pa = *pb;
  *pb = aux;
}

void Operador::intercambiar (int *pa, int *pb, int *pc){
  int aux = 0;
  aux = *pc;
  *pc = *pb;
  *pb = *pa;
  *pa = aux;
}
