#include "Estadistica.hpp"

double Estadistica::_foptimo = 0;
Solucion Estadistica::_ms;
double Estadistica::_tms = 0.0;
int Estadistica::_ims = 0;
double Estadistica::_tmr = 0.0;
clock_t Estadistica::_stmr = 0.0;
clock_t Estadistica::_ftmr = 0.0;
double Estadistica::_fp = 0;
double Estadistica::_fa = 0;
float Estadistica::_gap = 1.0;
float Estadistica::_agap = 0.0;
int Estadistica::_cs = 0;
int Estadistica::_ci = 0;
int Estadistica::_cev = 0;
int Estadistica::_crp = 0;
int Estadistica::_cpn = 0;
float Estadistica::_ef = 0.0;
int Estadistica::_valida = 0;

void Estadistica::inicializar(double foptimo){
  _foptimo = foptimo;
  _tms = 0.0;
  _ims = 0;
  _fp = 0;
  _fa = 0;
  _cev = 0;
  _crp = 0;
  _cpn = 0;
  _ef = 0.0;
}


void Estadistica::guardar_tms(float t){ _tms = t; }

void Estadistica::guardar_ims(int ims){ _ims = ims; }

void Estadistica::guardar_ms(Solucion s) { _ms = s; }

int Estadistica::gitr(){ return _ci; }

int Estadistica::gims(){ return _ims; }

void Estadistica::stimer(){ _stmr = clock(); }

void Estadistica::ftimer(){ _ftmr = clock(); }

float Estadistica::gtimer(){
  ftimer();
  return (_ftmr - _stmr) / (double) CLOCKS_PER_SEC;
}
void Estadistica::timer(){ _tmr = (_ftmr - _stmr) / (double) CLOCKS_PER_SEC;}

void Estadistica::mostrar_ms(){
  cout << "---------------------------------" << endl;
  cout << "FITNESS--: " << _ms.fitness() << endl;
  for(int i=0; i < _ms.size(); i++){
    if(*_ms.elemento(i) <= Problema::clientes()){
      cout << *_ms.elemento(i) << "|";
    }
    else{
      cout << "--" << *_ms.elemento(i) << "--|";
    }
  }
  cout << endl;
}

void Estadistica::escribir_ms(){
  ofstream file ("recorrido.m", ios::app);
  for(int i=0; i < _ms.size(); i++){
    if(*_ms.elemento(i) <= Problema::clientes()){
      file << *_ms.elemento(i) << " ";
    }
    else{
      file <<  "0 ";
    }
  }
  file << "\n";
  file.close();
}

Solucion * Estadistica::ms(){ return &_ms; }

void Estadistica::incrementar_ci() { _ci++; }

void Estadistica::incrementar_cs() { _cs++; }

void Estadistica::incrementar_cev(){ _cev++; }

void Estadistica::incrementar_crp(){ _crp++; }

void Estadistica::incrementar_cpn(){ _cpn++; }

void Estadistica::calcular_fp(){ _fp = _fa / _cs; }

void Estadistica::calcular_fa(double fa){ _fa = _fa + fa; }

void Estadistica::calcular_ef(){ _ef = (_cev - _crp) / _cev; }

void Estadistica::gap(){ _gap = (_ms.fitness() - _foptimo) / _foptimo; }

float Estadistica::gap_value(){ return _gap;}

void Estadistica::agap(){ _agap = (_fp - _foptimo) / _foptimo; }

float Estadistica::umbral(){ return (1 / _gap); }

void Estadistica::comparar(Solucion *s){
  if(_ms.fitness() == 0){
    _ms = *s;
    _tms = gtimer();
    _ims = gitr();
    // escribir_ms();
  }
  else{
    if(s->fitness() < _ms.fitness()){
      _tms = gtimer();
      _ims = gitr();
      _ms = *s;
      // escribir_ms();
    }
  }
}

void Estadistica::header(){
  ofstream infops ("datos_parciales.csv");
  infops << "# soluciones generadas;# iteraciones;#evaluaciones;# soluciones reparadas;gap;fitness promedio;tiempo transcurrido" << endl;
  infops.close();
}
void Estadistica::psumarize(){
  ofstream info ("datos_parciales.csv", ios::app);
  calcular_fp();
  gap();
  // info << "# soluciones generadas;# iteraciones;#evaluaciones;# soluciones penalizadas;gap;fitness promedio;tiempo transcurrido" << endl;
  info << _cs << ";" << _ci << ";" << _cev << ";" << _cpn << ";" << _gap << ";" << _fp << ";" << gtimer() << endl;
  info.close();
}

void Estadistica::sumarize(){
  ofstream info ("datos_finales.csv", ios::app);
  _valida = Problema::comprobar(_ms.vector());
  timer();
  calcular_fp();
  gap(); agap();
  info << _cs << ";" << _ci << ";" << _cev << ";" << _cpn  << ";" << _gap << ";" << _agap << ";" << _fp << ";" << _tmr << ";" << _ms.fitness() << ";" << _ims << ";" << _tms << ";" << _valida  << endl;
  info.close();
}
